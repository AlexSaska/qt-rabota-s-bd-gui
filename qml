import QtQuick 2.10
import QtQuick.Window 2.3
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import Qt.labs.handlers 1.0
import QtQuick.Controls.Styles 1.4

Window {
    id: window
    visible: true
    flags: Qt.FramelessWindowHint
    width: 510
    height: 300
    color: "#c3cb7d"
    opacity: 1
    title: qsTr("RGB Mixer")

    Connections {
        target: appCore

        onSendToQml: {
            labelCount.text = count
        }
    }

    RowLayout {
        anchors.centerIn: parent
    }

    Dial {
        id: dialR
        x: 52
        y: 50
        width: 100
        height: 100
        visible: true
        opacity: 1
    }

    Dial {
        id: dialG
        x: 199
        y: 50
        width: 100
        height: 100
    }

    Dial {
        id: dialB
        x: 343
        y: 50
        width: 100
        height: 100
    }

    Label {
        id: label
        x: 85
        y: 75
        color: "#2e3031"
        text: qsTr("R")
        font.bold: true
        font.pointSize: 33
    }

    Label {
        id: label1
        x: 231
        y: 75
        color: "#2e3031"
        text: qsTr("G")
        font.pointSize: 33
        font.bold: true
    }

    Label {
        id: label2
        x: 375
        y: 74
        color: "#2e3031"
        text: qsTr("B")
        font.pointSize: 33
        font.bold: true
    }

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 510
        height: 31
        color: "#2e3031"
    }

    Label {
        id: label3
        x: 2
        y: 3
        color: "#c3cb7d"
        text: qsTr("RGBMixer")
        font.bold: true
        font.pointSize: 14
        font.family: "Verdana"
    }

    Label {
        id: label4
        x: 488
        y: 5
        width: 15
        height: 18
        color: "#c3cb7d"
        text: qsTr("X")
        font.bold: true
        font.pointSize: 14
        lineHeight: 1.1
    }

    Rectangle {
        id: rectangle1
        x: 0
        y: 269
        width: 510
        height: 31
        color: "#2e3031"
    }

    Label {
        id: label5
        x: 441
        y: 273
        color: "#c3cb7d"
        text: qsTr("COM4")
        font.pointSize: 14
        font.family: "Verdana"
        font.bold: true
    }

    Button {
        id: button
        x: 85
        y: 192
        width: 125
        height: 40
        display: AbstractButton.TextBesideIcon

        contentItem: Text {
            x: 8
            color: "#c3cb7d"
            text: "Connection"
            renderType: Text.NativeRendering
            wrapMode: Text.WrapAnywhere
            //styleColor: "#60b2db"
            font.pointSize: 14
            font.bold: true
            font.family: "Times New Roman"
            //font: control.font
            opacity: enabled ? 1.0 : 0.3
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }


        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 40
            color: button.down ?  "#c3cb7d" : "#2e3031"
            border.color: "#2e3031"
            border.width: 2
            radius: 4
        }
    }

    Button {
        id: button1
        x: 284
        y: 192
        width: 125
        height: 40
        display: AbstractButton.TextBesideIcon

        contentItem: Text {
            x: 8
            color: "#c3cb7d"
            text: "Setting"
            renderType: Text.NativeRendering
            wrapMode: Text.WrapAnywhere
            //styleColor: "#60b2db"
            font.pointSize: 14
            font.bold: true
            font.family: "Times New Roman"
            //font: control.font
            opacity: enabled ? 1.0 : 0.3
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 40
            color: button1.down ?  "#c3cb7d" : "#2e3031"
            border.color: "#2e3031"
            border.width: 2
            radius: 4
        }
    }

    Label {
        id: label6
        x: 244
        y: 273
        color: "#c3cb7d"
        text: qsTr("settings")
        font.pointSize: 14
        font.family: "Verdana"
        font.bold: true
    }








}


